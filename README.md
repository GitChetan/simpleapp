# SimpleApp

## Introduction
- Created an simple standalone apllication using node .
- CI/CD helps automate the build, test, and deployment processes, leading to faster development cycles and improved software quality. 
- Basically there is auto updation in the deployed application. When the new change comes in the application, by doing a single task, whole application get modified and deployed by single commit in the project so no need to do redundant tasks


## Prerequisites
- Operating System (OS) : (e.g., windows, macOs or ubuntu etc)
- Version Control System (VCS) : (e.g., Github, Gitlab or Bitbucket)
- CI/CD Tools : (Jenkins, GitLab CI/CD, or GitHub Actions)
- Deployment Target: (like AWS, Azure, or GCP or as per the assignemnt it can be localhost.)
- Access Credentials: (e.g., SSH keys, API tokens, AWS IAM credentials).

## Installation Steps:

- Install and Configure CI/CD Tool: Install and configure your chosen CI/CD tool (e.g., Jenkins, GitLab CI/CD) on a server or cloud instance.

- Install Required Dependencies: Install any dependencies required by your CI/CD pipeline, such as Node.js, npm, Docker, etc

     Note : Above things can be installed from any browser on windows


## Configuration Details:
- Project Setup: Set up your project repository on your Version Control System and push your code to it.
 
- Configure CI/CD Pipeline: Define your CI/CD pipeline using a configuration file (e.g., .gitlab-ci.yml, Jenkinsfile) in the root of your project repository.
Define stages, jobs, and steps for building, testing, and deploying your application.

#### Code that I have used ( Jenkinsfile ) :

``` 

pipeline {
    agent any

    // Making jenkins check for changes on git repo every minute
    triggers {
        pollSCM '* * * * *'
    }

    stages {
        stage('Checkout') {
            steps {
                // Checkout the source code from the public GitLab repository
                checkout scm
                echo "Checkout done."
            }
        }

        stage('Testing') {
            steps {
                sh 'htmlhint index.html'
                echo "Testing.. done."
            }
        }

        stage('Build') {
            steps {
                sh 'npm --version'
                echo "Build.. done."
            }
        }

        stage('deploy'){
            steps{
                sh 'node app.js /dev/null 2>&1 &'
                echo "Deployment.. done."
            }
        }
    }
    options {
        timeout(time: 1, unit: 'MINUTES') // Set timeout to 1 minute
    }
}

```

In the above code, I added important commands to build, test and deploy node js application. 

- Secrets Management: Manage sensitive information such as access tokens, API keys, and passwords securely using built-in features of your CI/CD tool or external secret management solutions.

- Integration with Deployment Target: Configure your CI/CD pipeline to deploy your application to the target environment. This may involve setting up SSH, API, or other access methods.

- Webhook Configuration: Set up webhooks or triggers in your Version Control System to notify your CI/CD tool of new commits or changes in the repository.

![File to the host](screenShots/Webhook.png)

- You have to add trigger jenkins stage for the webhook configuration and CICD :
My gitlab-ci.yml file is :
```

image: node

stages:
    - build
    - test
    - deploy
    - trigger_jenkins

build_stage:
  stage: build
  script:
    - echo "Building.."
    - npm install
    - npm install -g htmlhint
  artifacts:
    paths:
      - node_modules
      - package-lock.json


run_test:
    stage: test
    image: node:latest
    before_script:
        - npm install -g htmlhint
    script:
        - echo "testing"
        - htmlhint index.html



deploy_stage:
  stage: deploy
  script:
    - echo "Deploying to test environment"
    - node app.js /dev/null 2>&1 &
  environment:
    name: test
    url: http://localhost:3000/ # URL of your test environment


trigger_jenkins:
  stage: trigger_jenkins
  script:
    - curl -X POST "http://3.101.40.208:8080/project/sample/webhook"

```

- Monitoring and Notifications: Set up monitoring and notification systems to track the progress of your CI/CD pipeline and receive alerts in case of failures or issues.


## Running the CI/CD Pipeline:

- Commit and Push Changes: Make changes to your project code and commit them to the repository. Push the changes to your Version Control System.

![File to the host](screenShots/Gitlab_Pipeline.png)

- Automatic Triggering: Your CI/CD pipeline should be automatically triggered by the webhook or trigger configured in the Version Control System. Basically, It should execute job in the jenkins project/intent.

- Monitor Pipeline Execution: Monitor the execution of your CI/CD pipeline in the CI/CD tool's dashboard or interface. Check the logs and output of each stage and job to ensure everything is running smoothly.

![File to the host](screenShots/Stage_View.png)

- Troubleshooting: In case of failures or issues, we can troubleshoot the problem by examining the logs, identifying the root cause, and making necessary adjustments to your pipeline configuration.

- Deployment and Verification: Once the pipeline completes successfully, verify that your application is deployed and functioning as expected in the target environment. Here we deployed on the localhost.

![File to the host](screenShots/Jenkins_Trigger.png)

For our case application will be deployed on http://localhost:3000 by the jenkins

![File to the host](screenShots/standalone_app.png)

***


## Workflow
Once the integration is set up, the workflow is as follows:
1. Make a commit to the GitLab repository.
2. The GitLab CI/CD pipeline will be triggered automatically.
3. The pipeline will make an API request to the Jenkins `/build` endpoint, triggering a Jenkins build.


## Usage
To use this project, follow these steps:
1. Clone the repository to your local machine.
2. Make any necessary changes to the `app.py` or `index.html` file or add new dependences or change the jenkins server name, project name and token name present in the url found in .gitlab-ci.yml.
3. Commit and push your changes to the GitLab repository.
4. The GitLab CI/CD pipeline will be triggered, which will in turn trigger a Jenkins build.




## Contributor
[https://gitlab.com/GitChetan/] - chetan Patil - WDGET2024014# SimpleApp
